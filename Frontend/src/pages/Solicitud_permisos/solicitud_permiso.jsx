
import React, { useState, useEffect, useRef } from 'react';
import { classNames } from 'primereact/utils';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { ProductService } from '../../components/service/ProductService';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Rating } from 'primereact/rating';
import { Toolbar } from 'primereact/toolbar';
import { InputTextarea } from 'primereact/inputtextarea';
import { RadioButton } from 'primereact/radiobutton';
import { InputNumber } from 'primereact/inputnumber';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Calendar } from 'primereact/calendar';
import axios from 'axios'; // Si estás utilizando axios

export default function Gestions(){

    let emptyProduct = {
        id: null,
        name: '',
        image: null,
        description: '',
        category: null,
        price: 0,
        quantity: 0,
        rating: 0,
        inventoryStatus: 'INSTOCK'
    };
    let entry_fam_empleado = {
        idfam_empleado: null,
        fa_nombre: '',
        fa_parentezco: '',
        fa_direccion: '',
        fa_lug_trabajo: ''
    };

    let entry_empleado ={
        idempleado: null,
        em_nombre: '',
        em_pri_apellido: '',
        em_seg_apellido: '',
        em_direccion: '',
        em_dpi: '',
        em_fecha_ini:'',
        em_pago_mens:'',
        em_fecha_nac:''
    }

    const [productDialog, setProductDialog] = useState(false);
    const [deleteProductDialog, setDeleteProductDialog] = useState(false);
    const [deleteProductsDialog, setDeleteProductsDialog] = useState(false);
    const [product, setProduct] = useState(emptyProduct);
    const [selectedProducts, setSelectedProducts] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const [data, setData] = useState([]);
    const [empleado, setEmpleado] = useState(entry_empleado)
    const [empleados, setEmpleados] = useState(null);
    const [fam_empleado, setFam_empleado] = useState([entry_fam_empleado])
    const [date13, setDate13] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);
    const productService = new ProductService();

    useEffect(() => {
        fetchDataFromApi();
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const formatCurrency = (value) => {
        return value.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
    }

    const openNew = () => {
        setEmpleado(entry_empleado);
        setSubmitted(false);
        setProductDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setProductDialog(false);
    }

    const hideDeleteProductDialog = () => {
        setDeleteProductDialog(false);
    }

    const hideDeleteProductsDialog = () => {
        setDeleteProductsDialog(false);
    }
    
    const fetchDataFromApi = async () => {
        try {
            // Realiza una solicitud GET a tu API local
            fetch('http://localhost:3001/empleado')
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Error al obtener datos de la API');
                    }
                })
                .then(data => {
                    setEmpleados(data.empleado);
                })
                .catch(error => {
                    console.error('Error al obtener datos:', error);
                });
            // Los datos de respuesta estarán en response.data
            const apiData = response.data;

            // Puedes hacer lo que necesites con los datos, por ejemplo, establecerlos en el estado
            setData(apiData);
        } catch (error) {
            console.error('Error al obtener datos desde la API:', error);
        }
    };


    const saveProduct = () => {
        setSubmitted(true);

        if (product.name.trim()) {
            let _products = [...products];
            let _product = {...product};
            if (product.id) {
                const index = findIndexById(product.id);

                _products[index] = _product;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'Product Updated', life: 3000 });
            }
            else {
                _product.id = createId();
                _product.image = 'product-placeholder.svg';
                _products.push(_product);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'Product Created', life: 3000 });
            }

            setProducts(_products);
            setProductDialog(false);
            setProduct(emptyProduct);
        }
    }

    const editProduct = (product) => {
        setProduct({...product});
        setProductDialog(true);
    }

    const confirmDeleteProduct = (product) => {
        setProduct(product);
        setDeleteProductDialog(true);
    }

    const deleteProduct = () => {
        let _products = products.filter(val => val.id !== product.id);
        setProducts(_products);
        setDeleteProductDialog(false);
        setProduct(emptyProduct);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'Product Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < products.length; i++) {
            if (products[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const importCSV = (e) => {
        const file = e.files[0];
        const reader = new FileReader();
        reader.onload = (e) => {
            const csv = e.target.result;
            const data = csv.split('\n');

            // Prepare DataTable
            const cols = data[0].replace(/['"]+/g, '').split(',');
            data.shift();

            const importedData = data.map(d => {
                d = d.split(',');
                const processedData = cols.reduce((obj, c, i) => {
                    c = c === 'Status' ? 'inventoryStatus' : (c === 'Reviews' ? 'rating' : c.toLowerCase());
                    obj[c] = d[i].replace(/['"]+/g, '');
                    (c === 'price' || c === 'rating') && (obj[c] = parseFloat(obj[c]));
                    return obj;
                }, {});

                processedData['id'] = createId();
                return processedData;
            });

            const _products = [...products, ...importedData];

            setProducts(_products);
        };

        reader.readAsText(file, 'UTF-8');
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteProductsDialog(true);
    }

    const deleteSelectedProducts = () => {
        let _products = products.filter(val => !selectedProducts.includes(val));
        setProducts(_products);
        setDeleteProductsDialog(false);
        setSelectedProducts(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'Products Deleted', life: 3000 });
    }

    const onCategoryChange = (e) => {
        let _product = {...product};
        _product['category'] = e.value;
        setProduct(_product);
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _product = {...product};
        _product[`${name}`] = val;

        setProduct(_product);
    }

    const onInputNumberChange = (e, name) => {
        const val = e.value || 0;
        let _product = {...product};
        _product[`${name}`] = val;

        setProduct(_product);
    }

    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <Button label="Nuevo" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>
            </React.Fragment>
        )
    }


    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-success mr-2" onClick={() => editProduct(rowData)} />
                <Button icon="pi pi-check-square" className="p-button-rounded p-button-info" onClick={() => confirmDeleteProduct(rowData)} />
            </React.Fragment>
        );
    }

    const header = (
        <div className="table-header">
            <h5 className="mx-0 my-1">Buscar</h5>
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );
    const productDialogFooter = (
        <React.Fragment>
            <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Guardar" icon="pi pi-check" className="p-button-text" onClick={saveProduct} />
        </React.Fragment>
    );
    const deleteProductDialogFooter = (
        <React.Fragment>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hideDeleteProductDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteProduct} />
        </React.Fragment>
    );
    const deleteProductsDialogFooter = (
        <React.Fragment>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hideDeleteProductsDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteSelectedProducts} />
        </React.Fragment>
    );

    return (
        <div className="datatable-crud-demo">
            <br></br>
            <Toast ref={toast} />

            <div className="card">
                <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                <DataTable ref={empleados} value={empleados} selection={selectedProducts} onSelectionChange={(e) => setSelectedProducts(e.value)}
                    dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]}
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} products"
                    globalFilter={globalFilter} header={header} responsiveLayout="scroll">
                    <Column selectionMode="multiple" headerStyle={{ width: '3rem' }} exportable={false}></Column>
                    <Column field="idempleado" header="Id" sortable style={{ minWidth: '8rem' }}></Column>
                    <Column field="em_nombre" header="Nombre" sortable style={{ minWidth: '20rem' }}></Column>
                    <Column field="em_pri_apellido" header="Apellido" body={(rowData) => (rowData.em_pri_apellido +" "+ rowData.em_seg_apellido)}></Column>
                    <Column field="em_dpi" header="Dpi"></Column>
                    <Column field="em_pago_mens" header="Salario"></Column>
                    <Column body={actionBodyTemplate} exportable={false} style={{ minWidth: '8rem' }}></Column>
                </DataTable>
            </div>





            <Dialog visible={productDialog} style={{ width: '450px' }} header="Datos de Empleado" modal className="p-fluid" footer={productDialogFooter} onHide={hideDialog}>
                <div className="field">
                    <label htmlFor="name">Dpi</label>
                    <InputText id="em_dpi" value={empleado.em_dpi} onChange={(e) => onInputChange(e, 'em_dpi')} required autoFocus className={classNames({ 'p-invalid': submitted && !empleado.em_dpi })} />
                </div>
                <div className="field">
                    <label htmlFor="name">Nombre</label>
                    <InputText id="em_nombre" value={empleado.em_nombre} onChange={(e) => onInputChange(e, 'em_nombre')} required  className={classNames({ 'p-invalid': submitted && !empleado.em_nombre })} />
                    {submitted && !empleado.em_nombre && <small className="p-error">Nombre es requerido.</small>}
                </div>
                <div className="field">
                    <label htmlFor="description">Apellido</label>
                    <InputText id="em_pri_apellido" value={empleado.em_pri_apellido} onChange={(e) => onInputChange(e, 'em_pri_apellido')} required className={classNames({ 'p-invalid': submitted && !empleado.em_nombre })}/>
                </div>
                <div className="field"> 
                    <label htmlFor="em_seg_apellido">Segundo Apellido</label>
                    <InputText id="em_seg_apellido" value={empleado.em_seg_apellido} onChange={(e) => onInputChange(e, 'em_seg_apellido')} required  className={classNames({ 'p-invalid': submitted && !empleado.em_nombre })} />
                </div>
                <div className="field">
                    <label htmlFor="icon">Fecha Nacimiento</label>
                    <Calendar id="icon" value={empleado.em_fecha_nac} onChange={(e) => setEmpleado(e.value)} showIcon />
                </div>
                <div className="field">
                    <label htmlFor="em_seg_apellido">Dirección</label>
                    <InputTextarea id="em_seg_apellido" value={empleado.em_seg_apellido} onChange={(e) => onInputChange(e, 'em_seg_apellido')} required  rows={1} cols={20}/>
                </div>
                <div className="field col-12 md:col-4">
                        <label htmlFor="touchUI">Fecha y Hora Inicio</label>
                        <Calendar id="touchUI" value={empleado.em_fecha_ini} onChange={(e) => setEmpleado(e.value)} touchUI showIcon className={classNames({ 'p-invalid': submitted && !empleado.em_nombre })}/>
                </div>
                <div className="formgrid grid">
                    <div className="field col">
                        <label htmlFor="price">Salario</label>
                        <InputNumber id="price" value={empleado.em_pago_mens} onValueChange={(e) => onInputNumberChange(e, 'price')} mode="currency" currency="USD" locale="en-US" />
                    </div>
                </div>
                <br></br>
                <div className="field">
                    <h5 className="mb-3">Datos Familiares:</h5>
                    <div className="formgrid grid" style={{padding:"5%"}}>
                        <div className="field col-6 md:col-4">
                            <label htmlFor="name">Nombre</label>
                            <InputText id="fa_nombre" value={fam_empleado.fa_nombre} onChange={(e) => onInputChange(e, 'fa_nombre')}  />
                        </div>
                        <div className="field col-6 md:col-4">
                            <label htmlFor="name">Parentezco</label>
                            <InputText id="fa_parentezco" value={fam_empleado.fa_parentezco} onChange={(e) => onInputChange(e, 'fa_parentezco')}  />
                        </div>
                        <div className="field col-6 md:col-4">
                            <label htmlFor="name">Direccion</label>
                            <InputText id="fa_direccion" value={fam_empleado.fa_direccion} onChange={(e) => onInputChange(e, 'fa_direccion')}  />
                        </div>
                        <div className="field col-6 md:col-4">
                            <label htmlFor="name">Lugar de trabajo</label>
                            <InputText id="fa_lug_trabajo" value={fam_empleado.fa_lug_trabajo} onChange={(e) => onInputChange(e, 'fa_lug_trabajo')}  />
                        </div>
                    </div>
                </div>

                
            </Dialog>

            <Dialog visible={deleteProductDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteProductDialogFooter} onHide={hideDeleteProductDialog}>
                <div className="confirmation-content">
                    <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem'}} />
                    {product && <span>¿Esta seguro de confirmar el permisos del empleado?<b>{product.name}</b></span>}
                </div>
            </Dialog>

            <Dialog visible={deleteProductsDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteProductsDialogFooter} onHide={hideDeleteProductsDialog}>
                <div className="confirmation-content">
                    <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem'}} />
                    {product && <span>Are you sure you want to delete the selected products?</span>}
                </div>
            </Dialog>
        </div>
    );
}
                 