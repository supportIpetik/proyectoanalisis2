import { useRouter } from 'next/router';
import React, { useState } from 'react';
import AppConfig from '../components/AppConfig';
import { Checkbox } from 'primereact/checkbox';
import { Button } from 'primereact/button';
import { Password } from 'primereact/password';
import { InputText } from 'primereact/inputtext';
import { classNames } from 'primereact/utils';

const login_principal = (props) => {
    const [email, setEmail] = useState('');
    const [contrasena, setPassword] = useState('');
    const [checked, setChecked] = useState(false);

    const router = useRouter();
    const containerClassName = classNames('surface-ground flex align-items-center justify-content-center min-h-screen min-w-screen overflow-hidden');

    const handleLogin = async () => {
        try {
          const response = await fetch('https://backanalisisdev-production.up.railway.app/login', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email, contrasena }),
          });
      
          if (response.ok) {
            // La solicitud fue exitosa, llama a la función de devolución de llamada
            // para cambiar el estado en el componente principal
            props.onLoginStatusChange(true);
      
            // Almacena los datos del usuario en el almacenamiento local
            const userData = await response.json();
            localStorage.setItem('user', JSON.stringify(userData.user));
          } else {
            // La solicitud falló, puedes manejar errores aquí
            alert('Inicio de sesión fallido');
          }
        } catch (error) {
          console.error('Error al iniciar sesión:', error);
          alert('Error al iniciar sesión');
        }
      };
      
    
    return (
        <div className="surface-ground flex align-items-center justify-content-center min-h-screen min-w-screen overflow-hidden">
            <div className="flex flex-column align-items-center justify-content-center">
                <div style={{ borderRadius: '56px', padding: '0.3rem', background: 'linear-gradient(180deg, var(--primary-color) 10%, rgba(33, 150, 243, 0) 30%)' }}>
                    <div className="w-full surface-card py-8 px-5 sm:px-8" style={{ borderRadius: '53px' }}>
                        <div className="text-center mb-5">
                            <div className="text-900 text-3xl font-medium mb-3">Bienvenidos!</div>
                            <span className="text-600 font-medium">Iniciar sesión para continuar</span>
                        </div>

                        <div>
                            <label htmlFor="email1" className="block text-900 text-xl font-medium mb-2">
                                Correo
                            </label>
                            <InputText inputid="email1" type="text" placeholder="Dirrección de correo" className="w-full md:w-30rem mb-5" style={{ padding: '1rem' }} value={email} onChange={(e) => setEmail(e.target.value)} />

                            <label htmlFor="password1" className="block text-900 font-medium text-xl mb-2">
                                Contraseña
                            </label>
                            <Password inputid="password1" value={contrasena} onChange={(e) => setPassword(e.target.value)} placeholder="Contraseña" toggleMask className="w-full mb-5" inputClassName="w-full p-3 md:w-30rem"></Password>

                            <div className="flex align-items-center justify-content-between mb-5 gap-5">
                                <a className="font-medium no-underline ml-2 text-right cursor-pointer" style={{ color: 'var(--primary-color)' }}>
                                </a>
                            </div>
                            <Button label="Iniciar Sesión" className="w-full p-3 text-xl" onClick={handleLogin}></Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

login_principal.getLayout = function getLayout(page) {
    return (
        <React.Fragment>
            {page}
            <AppConfig simple />
        </React.Fragment>
    );
};
export default login_principal;
