
import React, { useState, useEffect, useRef } from 'react';
import { classNames } from 'primereact/utils';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { ProductService } from '../../components/service/ProductService';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Rating } from 'primereact/rating';
import { Toolbar } from 'primereact/toolbar';
import { InputTextarea } from 'primereact/inputtextarea';
import { RadioButton } from 'primereact/radiobutton';
import { InputNumber } from 'primereact/inputnumber';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Calendar } from 'primereact/calendar';
import XLSX from 'sheetjs-style';
import { format, addDays, parse } from 'date-fns';
export default function Gestions() {

    let emptyProduct = {
        id: null,
        name: '',
        image: null,
        description: '',
        category: null,
        price: 0,
        quantity: 0,
        rating: 0,
        inventoryStatus: 'INSTOCK'
    };
    let entry_fam_empleado = {
        idfam_empleado: null,
        fa_nombre: '',
        fa_parentezco: '',
        fa_direccion: '',
        fa_lug_trabajo: '',
        empleado_idempleado: null
    };

    let entry_empleado = {
        idempleado: null,
        em_nombre: '',
        em_pri_apellido: '',
        em_seg_apellido: '',
        em_direccion: '',
        em_dpi: '',
        em_fecha_ini: '',
        em_pago_mens: '',
        em_fecha_nac: ''
    }

    const [productDialog, setProductDialog] = useState(false);
    const [datosFamiliaresDialog, setDatosFamiliaresDialog] = useState(false);
    const [deleteProductDialog, setDeleteProductDialog] = useState(false);
    const [deleteProductsDialog, setDeleteProductsDialog] = useState(false);
    const [product, setProduct] = useState(emptyProduct);
    const [selectedProducts, setSelectedProducts] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const [data, setData] = useState([]);
    const [empleado, setEmpleado] = useState(entry_empleado)
    const [empleados, setEmpleados] = useState(null);
    const [fam_empleado, setFam_empleado] = useState(entry_fam_empleado)
    const [famEmpleadoData, setFamEmpleadoData] = useState([]);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        fetchDataFromApi();
    }, []); // eslint-disable-line react-hooks/exhaustive-deps


    const openNew = () => {
        setEmpleado(entry_empleado);
        setSubmitted(false);
        setProductDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setProductDialog(false);
    }
    const hideDialog2 = () => {
        setSubmitted(false);
        setDatosFamiliaresDialog(false);
    }


    const hideDeleteProductDialog = () => {
        setDeleteProductDialog(false);
    }

    const hideDeleteProductsDialog = () => {
        setDeleteProductsDialog(false);
    }

    const fetchDataFromApi = async () => {
        try {
            // Realiza una solicitud GET a tu API local
            fetch('https://backanalisisdev-production.up.railway.app/empleado')
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Error al obtener datos de la API');
                    }
                })
                .then(data => {
                    console.log(data.empleado)
                    setEmpleados(data.empleado);
                })
                .catch(error => {
                    console.error('Error al obtener datos:', error);
                });
        } catch (error) {
            console.error('Error al obtener datos desde la API:', error);
        }
    };


    const saveProduct = () => {
        setSubmitted(true);
        if ( empleado.em_nombre.trim() || empleado.em_pago_mens.trim() || empleado.em_direccion.trim()) {
            let _user = { ...empleado };
            _user.em_fecha_ini = format(_user.em_fecha_ini, "yyyy/MM/dd HH:mm:ss");
            _user.em_fecha_nac = format(_user.em_fecha_nac, "yyyy/MM/dd HH:mm:ss");
            if (_user.idempleado) {

                // Realizar aquí la solicitud de actualización a la API utilizando fetch
                fetch(`https://backanalisisdev-production.up.railway.app/update_empleado/${_user.idempleado}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(_user),
                })
                    .then(response => {
                        if (response.ok) {
                            fetchDataFromApi();
                            return response.json();
                        } else {
                            throw new Error('Error al actualizar el usuario');
                        }
                    })
                    .then(data => {
                        fetchDataFromApi();
                        setProductDialog(false);
                    })
                    .catch(error => {
                        setProductDialog(false);
                        toast.current.show({ severity: 'war', summary: 'Error', detail: 'Error al actualizar al Empleado', life: 3000 });
                    });
                toast.current.show({ severity: 'success', summary: 'Exitoso', detail: 'Usuario Actualizado', life: 3000 });
            } else {
                console.log(_user)
                // Realizar aquí la solicitud de creación a la API utilizando fetch
                fetch('https://backanalisisdev-production.up.railway.app/crear_empleado', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(_user),
                })
                    .then(response => {
                        if (response.ok) {
                            return response.json();
                        } else {
                            throw new Error('Error al crear el usuario');
                        }
                    })
                    .then(data => {
                        // Manejar la respuesta de la API si es necesario
                        fetchDataFromApi();
                        setProductDialog(false);
                        toast.current.show({ severity: 'success', summary: 'Exitoso', detail: 'Empleado Creado', life: 3000 });
                    })
                    .catch(error => {
                        setProductDialog(false);
                        toast.current.show({ severity: 'war', summary: 'Error', detail: 'Error al crear al Empleado', life: 3000 });
                    });

            }
        }
    };

    const saveFam = () => {
        setSubmitted(true);
        if (fam_empleado.fa_nombre.trim() || fam_empleado.fa_parentezco.trim() || fam_empleado.fa_direccion.trim() || fam_empleado.fa_lug_trabajo.trim()) {
            let _user = { ...fam_empleado };
            _user.empleado_idempleado = empleado.idempleado;
            console.log(_user)
            // Realizar aquí la solicitud de creación a la API utilizando fetch
            fetch('https://backanalisisdev-production.up.railway.app/crear_fam_empleado', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(_user),
            })
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Error al crear el usuario');
                    }
                })
                .then(data => {
                    // Manejar la respuesta de la API si es necesario
                    fetchDataFromApi();
                    setDatosFamiliaresDialog(false);
                    toast.current.show({ severity: 'success', summary: 'Exitoso', detail: 'Familiar Creado', life: 3000 });

                })
                .catch(error => {
                    setDatosFamiliaresDialog(false);
                    toast.current.show({ severity: 'war', summary: 'Error', detail: 'Error al crear al Familiar', life: 3000 });
                });

        }
    };
    const editProduct = (product) => {
        console.log(product)
        product.em_fecha_ini = format(addDays(new Date(product.em_fecha_ini), 1), 'dd/MM/yy');
        product.em_fecha_nac = format(addDays(new Date(product.em_fecha_nac), 1), 'dd/MM/yy');
        product.em_fecha_ini = parse(product.em_fecha_ini, 'dd/MM/yy', new Date());
        product.em_fecha_nac = parse(product.em_fecha_nac, 'dd/MM/yy', new Date());
        console.log(product)
        setEmpleado({ ...product });
        setProductDialog(true);
    }
    const datosNewFamiliares = () => {
        setDatosFamiliaresDialog(true);
    }
    const confirmDeleteProduct = (empleadodelete) => {
        setEmpleado({
            idempleado: empleadodelete.idempleado,
            em_nombre: empleadodelete.em_dpi,
        })
        setDeleteProductDialog(true);
    }

    const deleteEmpleado = () => {
        // Realiza una solicitud al servidor para eliminar el usuario
        fetch(`https://backanalisisdev-production.up.railway.app/delete_empleado/${empleado.idempleado}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((response) => {
                if (response.ok) {
                    // Elimina el usuario de la lista en el cliente
                    const updatedUsers = users.filter((u) => u.id !== user.id);
                    setUsers(updatedUsers);
                    setDeleteUserDialog(false);
                    setUser(emtyUsers);
                    toast.current.show({ severity: 'success', summary: 'Successful', detail: 'User Deleted', life: 3000 });
                } else {
                    // Maneja errores si es necesario
                    console.error('Error al eliminar el usuario:', response.status);
                }
            })
            .catch((error) => {
                console.error('Error al eliminar el usuario:', error);
            });
    }

    const generarExcel = () => {
        try {
            // Realiza una solicitud GET a tu API local
            fetch(`https://backanalisisdev-production.up.railway.app/empleado`)
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Error al obtener los datos de los empleados');
                    }
                })
                .then(data => {
                    // Actualiza el estado con los datos obtenidos
                    console.log(data.empleado)
                    setFamEmpleadoData(data.empleado);
                })
                .catch(error => {
                    console.error(error);
                    // Maneja el error de la solicitud
                });
        } catch (error) {
            console.error('Error al obtener datos desde la API:', error);
        }
        const encabezadoTotal = [['Informe Empleados', '', '', '', '', '', '', '', '', '', '', '', '', '', '']];
        const encabezados = ['Dpi', 'Nombre', 'Primer Apellido', 'Segundo Apellido', 'Fecha Nacimiento', 'Salario', 'Dirección', 'Fecha Inicio'];

        const estilo = {
            font: { bold: false },
            alignment: { horizontal: 'center', vertical: 'center' },
            border: {
                top: { style: 'thin', color: { auto: 1 } },
                bottom: { style: 'thin', color: { auto: 1 } },
                left: { style: 'thin', color: { auto: 1 } },
                right: { style: 'thin', color: { auto: 1 } },
            },
        };
        console.log(famEmpleadoData + "antes de subirse")

        const filas = famEmpleadoData.map(empleadodata => [
              empleadodata.em_dpi,
              empleadodata.em_nombre,
              empleadodata.em_pri_apellido,
              empleadodata.em_seg_apellido,
              format(addDays(new Date(empleadodata.em_fecha_nac), 1), 'dd/MM/yy'),
              empleadodata.em_pago_mens,
              empleadodata.em_direccion,
              format(addDays(new Date(empleadodata.em_fecha_ini), 1), 'dd/MM/yy'),
            ]);
          

       /*  // Crear una nueva hoja para datos familiares
        const familiaresEncabezados = ['Id Empleado','Nombre', 'Parentezco', 'Dirección', 'Lugar de Trabajo'];
        const familiaresFilas = famEmpleadoData.map(familiar => [
            familiar.empleado_idempleado,
            familiar.fa_nombre,
            familiar.fa_parentezco,
            familiar.fa_direccion,
            familiar.fa_lug_trabajo,
        ]); */

        const archivoExcel = XLSX.utils.book_new();


        // Resto del código para la hoja de datos del empleado
        const hojaEmpleado = XLSX.utils.aoa_to_sheet([...encabezadoTotal, encabezados, ...filas]);
        hojaEmpleado['!cols'] = [{ width: 20 }, { width: 20 }, { width: 30 }, { width: 30 }, { width: 30 }, { width: 20 }, { width: 20 }, { width: 30 }];
        hojaEmpleado['!rows'] = filas.map(() => ({ height: 30 }));
        hojaEmpleado['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: encabezados.length - 1 }, style: { font: { size: 26 }, ...estilo } }];

        Object.keys(hojaEmpleado).forEach(key => {
            if (/^[A-Z]+\d+$/.test(key)) {
                hojaEmpleado[key].s = estilo;
            }
        });

        // Agregar la hoja de datos del empleado al libro de trabajo
        XLSX.utils.book_append_sheet(archivoExcel, hojaEmpleado, 'Empleado');
        XLSX.writeFile(archivoExcel, 'empleado_.xlsx');
    };



    const confirmDeleteSelected = () => {
        setDeleteProductsDialog(true);
    }

    const deleteSelectedProducts = () => {
        let _products = products.filter(val => !selectedProducts.includes(val));
        setProducts(_products);
        setDeleteProductsDialog(false);
        setSelectedProducts(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'Products Deleted', life: 3000 });
    }

    const onCategoryChange = (e) => {
        let _product = { ...product };
        _product['category'] = e.value;
        setProduct(_product);
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _product = { ...empleado };
        _product[`${name}`] = val;

        setEmpleado(_product);
    }
    const onInputChangeFam = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _product = { ...fam_empleado };
        _product[`${name}`] = val;

        setFam_empleado(_product);
    }

    const onInputNumberChange = (e, name) => {
        const val = e.value || 0;
        let _product = { ...product };
        _product[`${name}`] = val;

        setProduct(_product);
    }

    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <FileUpload 
                    mode="basic"
                    name="demo[]"
                    accept=".xlsx"
                    maxFileSize={1000000}
                    label="Importar"
                    chooseLabel="Importar"
                    className="mr-2"
                    onSelect={onImportExcel}
                    auto={false} // Establecer auto a false para prevenir el auto procesamiento
                />
                <Button label="Exportar" icon="pi pi-file-export" severity="help" className="p-button-raised" onClick={() => generarExcel()} />
            </React.Fragment>
        )
    }

    function limpiarObjeto(obj) {
        const objetoLimpio = {};
        Object.keys(obj).forEach(key => {
          if (obj[key] !== undefined && obj[key] !== null) {
            objetoLimpio[key] = obj[key];
          }
        });
        return objetoLimpio;
      }
      
    
      const onImportExcel = (e) => {
        if (e.files && e.files[0]) {
            const file = e.files[0]; // Obtener el archivo seleccionado
            const reader = new FileReader();
            reader.onload = async (event) => { // Asegúrate de que esta función sea asíncrona
                const data = new Uint8Array(event.target.result);
                const workbook = XLSX.read(data, { type: 'array' });
    
                const sheetName = workbook.SheetNames[0];
                const worksheet = workbook.Sheets[sheetName];
                const json = XLSX.utils.sheet_to_json(worksheet, { header: 1 }).slice(1);
                
                const headers = json[0];
                const dataRows = json.slice(1);
    
                const dataObjects = dataRows.map(row => {
                    let obj = {};
                    headers.forEach((header, index) => {
                        if (row[index] !== undefined && row[index] !== null) { // Asegura que no se agreguen valores undefined o null
                            obj[header] = row[index];
                        }
                    });
                    return limpiarObjeto(obj); // Limpia el objeto antes de agregarlo al array
                }).filter(obj => Object.keys(obj).length > 0); // Filtra objetos vacíos
    
                console.log(dataObjects); // Datos procesados para enviar
    
                // Envío de los datos al backend
                try {
                    const response = await fetch('https://backanalisisdev-production.up.railway.app/importar_empleados', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(dataObjects), // Convertir los datos del objeto a una cadena JSON
                    });
    
                    if (!response.ok) {
                        throw new Error('Algo salió mal al enviar los datos al servidor');
                    }
    
                    const result = await response.json(); // Suponiendo que tu backend responde con JSON
                    console.log(result); // Manejo de la respuesta del servidor
                    fetchDataFromApi();
                    alert('Datos importados exitosamente');
                } catch (error) {
                    console.error('Error al enviar los datos:', error);
                    alert('Error al importar datos');
                }
            };
            reader.readAsArrayBuffer(file);
        }
    };
    
    
    
    
    

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

            </React.Fragment>
        )
    }


    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-success mr-2" onClick={() => editProduct(rowData)} />{/* 
                <Button icon="pi pi-file-export" className="p-button-rounded p-button-help" onClick={() => generarExcel(rowData)} /> */}
            </React.Fragment>
        );
    }

    const header = (
        <div className="table-header">
            <h5 className="mx-0 my-1">Buscar...</h5>
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Buscar..." />
            </span>
        </div>
    );
    const productDialogFooter = (
        <React.Fragment>
            <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Guardar" icon="pi pi-check" className="p-button-text" onClick={saveProduct} />
        </React.Fragment>
    );
    const datosFamiliaresDialogFooter = (
        <React.Fragment>
            <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={hideDialog2} />
            <Button label="Guardar" icon="pi pi-check" className="p-button-text" onClick={saveFam} />
        </React.Fragment>
    );
    const deleteProductDialogFooter = (
        <React.Fragment>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hideDeleteProductDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteEmpleado} />
        </React.Fragment>
    );
    const deleteProductsDialogFooter = (
        <React.Fragment>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hideDeleteProductsDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteSelectedProducts} />
        </React.Fragment>
    );

    return (
        <div className="datatable-crud-demo">
            <br></br>
            <Toast ref={toast} />

            <div className="card">
                <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                <DataTable ref={empleados} value={empleados} selection={selectedProducts} onSelectionChange={(e) => setSelectedProducts(e.value)}
                    dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]}
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} products"
                    globalFilter={globalFilter} header={header} responsiveLayout="scroll">
                    <Column selectionMode="multiple" headerStyle={{ width: '3rem' }} exportable={false}></Column>{/* 
                    <Column field="idempleado" header="Id" sortable style={{ minWidth: '8rem' }}></Column> */}
                    <Column field="em_nombre" header="Nombre" sortable style={{ minWidth: '10rem' }}></Column>
                    <Column field="em_pri_apellido" header="Apellido" style={{ minWidth: '10rem' }} body={(rowData) => (rowData.em_pri_apellido + " " + rowData.em_seg_apellido)}></Column>
                    <Column field="em_dpi" header="Dpi"></Column>
                    <Column
                        field="em_pago_mens"
                        header="Salario"
                        sortable
                        body={(rowData) => (
                            <span style={{ color: 'orange' }}>
                                Q. {parseFloat(rowData.em_pago_mens).toLocaleString('es-US', {
                                    minimumFractionDigits: 2,
                                    maximumFractionDigits: 2,
                                })}
                            </span>
                        )}
                    />
                    <Column body={actionBodyTemplate} exportable={false} style={{ minWidth: '8rem' }}></Column>
                </DataTable>
            </div>





            <Dialog visible={productDialog} style={{ width: '450px' }} header="Datos de Empleado" modal className="p-fluid" footer={productDialogFooter} onHide={hideDialog}>
                <div className="field">
                    <label htmlFor="name">Dpi</label>
                    <InputNumber inputId="em_dpi" value={empleado.em_dpi} onValueChange={(e) => onInputChange(e, 'em_dpi')} mode="decimal" useGrouping={false} />
                    {submitted && !empleado.em_dpi && <small className="p-error">Dpi es requerido.</small>}
                </div>
                <div className="field">
                    <label htmlFor="name">Nombre</label>
                    <InputText id="em_nombre" value={empleado.em_nombre} onChange={(e) => onInputChange(e, 'em_nombre')} required className={classNames({ 'p-invalid': submitted && !empleado.em_nombre })} />
                    {submitted && !empleado.em_nombre && <small className="p-error">Nombre es requerido.</small>}
                </div>
                <div className="field">
                    <label htmlFor="description">Apellido</label>
                    <InputText id="em_pri_apellido" value={empleado.em_pri_apellido} onChange={(e) => onInputChange(e, 'em_pri_apellido')} required className={classNames({ 'p-invalid': submitted && !empleado.em_pri_apellido })} />
                    {submitted && !empleado.em_pri_apellido && <small className="p-error">Apellido es requerido.</small>}
                </div>
                <div className="field">
                    <label htmlFor="em_seg_apellido">Segundo Apellido</label>
                    <InputText id="em_seg_apellido" value={empleado.em_seg_apellido} onChange={(e) => onInputChange(e, 'em_seg_apellido')} required className={classNames({ 'p-invalid': submitted && !empleado.em_seg_apellido })} />
                    {submitted && !empleado.em_seg_apellido && <small className="p-error">Segundo Apellido es requerido.</small>}
                </div>
                <div className="field">
                    <label htmlFor="em_fecha_nac">Fecha Nacimiento</label>
                    <Calendar id="em_fecha_nac" dateFormat="dd/mm/yy" value={empleado.em_fecha_nac} onChange={(e) => onInputChange(e, 'em_fecha_nac')} required showIcon className={classNames({ 'p-invalid': submitted && !empleado.em_fecha_nac })}/>
                    {submitted && !empleado.em_fecha_nac && <small className="p-error">Fecha Nacimiento es requerido.</small>}
                </div>
                <div className="field">
                    <label htmlFor="em_direccion">Dirección</label>
                    <InputTextarea id="em_direccion" value={empleado.em_direccion} onChange={(e) => onInputChange(e, 'em_direccion')} required rows={1} cols={20} className={classNames({ 'p-invalid': submitted && !empleado.em_direccion })}/>
                    {submitted && !empleado.em_direccion && <small className="p-error">Dirección es requerido.</small>}
                </div>
                <div className="field">
                    <label htmlFor="em_fecha_ini">Fecha Inicio</label>
                    <Calendar id="em_fecha_ini" dateFormat="dd/mm/yy" value={empleado.em_fecha_ini} onChange={(e) => onInputChange(e, 'em_fecha_ini')} required showIcon className={classNames({ 'p-invalid': submitted && !empleado.em_fecha_ini })} />
                    {submitted && !empleado.em_fecha_ini && <small className="p-error">Fecha Inicio es requerido.</small>}
                </div>
                <div className="formgrid grid">
                    <div className="field col">
                        <label htmlFor="em_pago_mens">Salario</label>
                        <InputNumber id="em_pago_mens" value={empleado.em_pago_mens} onValueChange={(e) => onInputChange(e, 'em_pago_mens')} required   mode="currency" currency="GTQ" locale="en-US" className={classNames({ 'p-invalid': submitted && !empleado.em_pago_mens })}/>
                        {submitted && !empleado.em_pago_mens && <small className="p-error">Salario es requerido.</small>}
                    </div>
                </div>{/* 
                {empleado.idempleado ? (
                    <div className="field">
                        <label htmlFor="touchUI">Agregar Datos Familiares</label>

                        <div className="field col-2">
                            <Button icon="pi pi-user-plus" className=" p-button-lg p-button-rounded p-button-help" aria-label="Notification" onClick={datosNewFamiliares} />
                        </div>
                    </div>
                ) : (
                    <div className="field">
                        <label htmlFor="touchUI">---</label>
                    </div>
                )} */}


            </Dialog>
            <Dialog visible={datosFamiliaresDialog} style={{ width: '450px' }} modal className="p-fluid" footer={datosFamiliaresDialogFooter} onHide={hideDialog2}>
                <div className="field">
                    <h5 className="mb-3">Datos Familiares:</h5>
                    <div className="formgrid grid" style={{ padding: "5%" }}>
                        <div className="field col-6 md:col-4">
                            <label htmlFor="name">Nombre</label>
                            <InputText id="fa_nombre" value={fam_empleado.fa_nombre} required onChange={(e) => onInputChangeFam(e, 'fa_nombre')}  className={classNames({ 'p-invalid': submitted && !fam_empleado.fa_nombre })}/>
                        </div>
                        <div className="field col-6 md:col-4">
                            <label htmlFor="name">Parentezco</label>
                            <InputText id="fa_parentezco" value={fam_empleado.fa_parentezco} required onChange={(e) => onInputChangeFam(e, 'fa_parentezco')} className={classNames({ 'p-invalid': submitted && !fam_empleado.fa_parentezco })}/>
                        </div>
                        <div className="field col-6 md:col-4">
                            <label htmlFor="name">Direccion</label>
                            <InputText id="fa_direccion" value={fam_empleado.fa_direccion} required onChange={(e) => onInputChangeFam(e, 'fa_direccion')} className={classNames({ 'p-invalid': submitted && !fam_empleado.fa_direccion })}/>
                        </div>
                        <div className="field col-6 md:col-4">
                            <label htmlFor="name">Lugar de trabajo</label>
                            <InputText id="fa_lug_trabajo" value={fam_empleado.fa_lug_trabajo} required onChange={(e) => onInputChangeFam(e, 'fa_lug_trabajo')} className={classNames({ 'p-invalid': submitted && !fam_empleado.fa_lug_trabajo })}/>
                        </div>
                    </div>
                </div>
            </Dialog>

            <Dialog visible={deleteProductDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteProductDialogFooter} onHide={hideDeleteProductDialog}>
                <div className="confirmation-content">
                    <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                    {empleado && <span>Esta seguro en eliminar al empleado con Dpi:  <b>{empleado.em_dpi}</b>?</span>}
                </div>
            </Dialog>

            <Dialog visible={deleteProductsDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteProductsDialogFooter} onHide={hideDeleteProductsDialog}>
                <div className="confirmation-content">
                    <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                    {product && <span>Are you sure you want to delete the selected products?</span>}
                </div>
            </Dialog>
        </div>
    );
}
