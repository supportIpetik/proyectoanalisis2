import React, { useState, useEffect } from 'react';
import { Card } from 'primereact/card';

export default function PaginationDemo() {
    const [userData, setUserData] = useState(null);
    const [userRol, setUserRol] = useState(null);
    useEffect(() => {
        const storedUserData = localStorage.getItem('user');
        console.log('Stored User Data:', storedUserData); // Verifica lo que se obtiene de localStorage
        if (storedUserData) {
            const userData = JSON.parse(storedUserData);
            setUserData(userData);
            setUserRol(userData.role_id);
        }
    }, [])

    return (
        <div className="d-flex justify-content-center align-items-center" style={{ height: '100vh' }}>
            <div>
                <Card title="Bienvenido al Sistema para IMPORTAR Y EXPORTAR" style={{ width: '35rem', marginBottom: '2em' }}>
                    <h5 className="text-center">ImportExport</h5>
                    <br></br>
                    {userData && <span>Usuario: <b>{userData.nombre}</b></span>}
                    <div  className="text-right" >
                        <img src="/logo.jpg" alt="..." className="rounded-circle" style={{ width: '150px', height: '150px' }} />
                    </div>
                </Card>
            </div>
        </div>

    );
}
