
import React, { useState, useEffect, useRef } from 'react';
import { classNames } from 'primereact/utils';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { ProductService } from '../../components/service/ProductService';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Rating } from 'primereact/rating';
import { Toolbar } from 'primereact/toolbar';
import { InputTextarea } from 'primereact/inputtextarea';
import { RadioButton } from 'primereact/radiobutton';
import { InputNumber } from 'primereact/inputnumber';
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Calendar } from 'primereact/calendar';
import GooeyButton from './button';
import { Dropdown } from 'primereact/dropdown';
import axios from 'axios'; // Si estás utilizando axios
import { format, addDays } from 'date-fns';
import XLSX from 'sheetjs-style';
import { Menu } from 'primereact/menu';
import { SplitButton } from 'primereact/splitbutton';
export default function Gestions() {

    let emptyProduct = {
        id: null,
        name: '',
        image: null,
        description: '',
        category: null,
        price: 0,
        quantity: 0,
        rating: 0,
        inventoryStatus: 'INSTOCK'
    };
    let entry_fam_empleado = {
        idfam_empleado: null,
        fa_nombre: '',
        fa_parentezco: '',
        fa_direccion: '',
        fa_lug_trabajo: ''
    };

    let entry_horas = {
        idhoras_extras: null,
        empleado_idempleado: null,
        ho_fecha: null,
        ho_dia_festivo: false,
        horas_extrascol: 0
    }
    let entry_hora_ingreso = {
        id_ingreso: null,
        fecha: null,
        hora_entrada: null,
        hora_salida: null,
        empleado_id: null,
        descripcion: ''
    }
    let entry_Tienda = {
        idtienda_emp: null,
        te_cuotas: 0,
        te_monto: 0,
        te_descripcion: ' ',
        te_monto_cuotas: 0,
        empleado_idempleado: null
    }
    let entryprestamo_banc = {
        idprestamo_banc: null,
        pr_cuotas: 0,
        pr_monto: 0,
        pr_descripcion: '',
        pr_monto_cuotas: 0,
        empleado_idempleado: null
    }
    const [visible, setVisible] = useState(false);
    const [visible2, setVisible2] = useState(false);
    const [visible3, setVisible3] = useState(false);
    const [visible4, setVisible4] = useState(false);
    const [visible5, setVisible5] = useState(false);
    const [visible6, setVisible6] = useState(false);
    const [productDialog, setProductDialog] = useState(false);
    const [insertarHora_ingresoDialog, setInsertarHora_ingresoDialog] = useState(false);
    const [insertarDeudaTiendaDialog, setInsertarDeudaTiendaDialog] = useState(false);
    const [insertprestamoBanc, setInsertprestamoBanc] = useState(false);
    const [deleteProductDialog, setDeleteProductDialog] = useState(false);
    const [deleteProductsDialog, setDeleteProductsDialog] = useState(false);
    const [product, setProduct] = useState(emptyProduct);
    const [selectedProducts, setSelectedProducts] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const [data, setData] = useState([]);
    const [horas_extras, setHoras_extas] = useState(entry_horas)
    const [hora_ingreso, setHora_Ingreso] = useState(entry_hora_ingreso)
    const [tienda_deuda, setTienda_deuda] = useState(entry_Tienda)
    const [prestamo_banc, setPrestamo_banc] = useState(entryprestamo_banc)
    const [tienda_moto_cuotas, setTienda_moto_cuotas] = useState(null)
    const [pr_moto_cuotas, setPr_moto_cuotas] = useState(null)
    const [empleados, setEmpleados] = useState(null);
    const [fam_empleado, setFam_empleado] = useState([entry_fam_empleado])
    const [famEmpleadoDataBono, setFamEmpleadoDataBono] = useState([]);
    const [famEmpleadoDataAgui, setFamEmpleadoDataAgui] = useState([]);
    const [famEmpleadoDataIggs, setFamEmpleadoDataIggs] = useState([]);
    const [famEmpleadoDataES, setFamEmpleadoDataES] = useState([]);
    const toast = useRef(null);
    const dt = useRef(null);
    const [roles, setRoles] = useState([]);
    const [selectedRole, setSelectedRole] = useState(null);
    const productService = new ProductService();
    const menu = useRef(null);

    useEffect(() => {
        fetchDataFromApi();
        optenerRoles()
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const accept = () => {
        generarQuincena()
        toast.current.show({ severity: 'success', summary: 'Confirmado', detail: 'cálculo de Quincena realizado', life: 5000 });
    }

    const reject = () => {
        toast.current.show({ severity: 'warn', summary: 'Cancelado', detail: 'No se realizo el cálculo de la Quincena', life: 5000 });
    }
    const accept2 = () => {
        generarFinMes()
        toast.current.show({ severity: 'success', summary: 'Confirmado', detail: 'cálculo de Fin de Mes realizado', life: 5000 });
    }

    const reject2 = () => {
        toast.current.show({ severity: 'warn', summary: 'Cancelado', detail: 'No se realizo el cálculo de Fin de Mes', life: 5000 });
    }
    const accept3 = () => {
        generarBono()

        toast.current.show({ severity: 'success', summary: 'Confirmado', detail: 'cálculo de Bono realizado', life: 5000 });
    }

    const reject3 = () => {
        toast.current.show({ severity: 'warn', summary: 'Cancelado', detail: 'No se realizo el cálculo del Bono', life: 5000 });
    }
    const acceptgenerarAguinaldo = () => {
        generarAguinaldo()
        toast.current.show({ severity: 'success', summary: 'Confirmado', detail: 'cálculo de Aguinaldo realizado', life: 5000 });
    }

    const rejectgenerarAguinaldo = () => {
        toast.current.show({ severity: 'warn', summary: 'Cancelado', detail: 'No se realizo el cálculo de Aguinaldo', life: 5000 });
    }

    const accept5 = () => {
        optenerListIggs()
        toast.current.show({ severity: 'success', summary: 'Confirmado', detail: 'Reporte de Iggs realizado', life: 5000 });
    }

    const reject5 = () => {
        toast.current.show({ severity: 'warn', summary: 'Cancelado', detail: 'No se realizo el Reporte de Iggs', life: 5000 });
    }

    const accept6 = () => {
        optenerListES()
        
    }

    const reject6 = () => {
        toast.current.show({ severity: 'warn', summary: 'Cancelado', detail: 'No se realizo el Reporte de Entradas y Salidas del empleado', life: 5000 });
    }

    const hideDialog = () => {
        setSubmitted(false);
        setProductDialog(false);
    }
    const hideDialog2 = () => {
        setSubmitted(false);
        setInsertarDeudaTiendaDialog(false);
    }
    const hideDialog3 = () => {
        setSubmitted(false);
        setInsertprestamoBanc(false);
    }
    const hideDialog4 = () => {
        setSubmitted(false);
        setInsertarHora_ingresoDialog(false);
    }
    const hideDeleteProductDialog = () => {
        setDeleteProductDialog(false);
    }

    const hideDeleteProductsDialog = () => {
        setDeleteProductsDialog(false);
    }
    const generarQuincena = () => {
        try {
            fetch('https://backanalisisdev-production.up.railway.app/calcular_quincena')
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Error al obtener datos de la API');
                    }
                })
                .then(data => {

                })
                .catch(error => {
                    console.error('Error al obtener datos:', error);
                });
        } catch (error) {
            console.error('Error al obtener datos desde la API:', error);
        }
    }
    const generarFinMes = () => {
        try {
            fetch('https://backanalisisdev-production.up.railway.app/calcular_fin_mes')
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Error al obtener datos de la API');
                    }
                })
                .then(data => {

                })
                .catch(error => {
                    console.error('Error al obtener datos:', error);
                });
        } catch (error) {
            console.error('Error al obtener datos desde la API:', error);
        }
    }
    const generarBono = () => {
        try {
            fetch('https://backanalisisdev-production.up.railway.app/calcular_bono')
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Error al obtener datos de la API');
                    }
                })
                .then(data => {
                    optenerListAbono()
                })
                .catch(error => {
                    console.error('Error al obtener datos:', error);
                });
        } catch (error) {
            console.error('Error al obtener datos desde la API:', error);
        }
    }
    const generarAguinaldo = () => {
        try {
            fetch('https://backanalisisdev-production.up.railway.app/calcular_faguinaldo')
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Error al obtener datos de la API');
                    }
                })
                .then(data => {
                    optenerListAgui()
                })
                .catch(error => {
                    console.error('Error al obtener datos:', error);
                });
        } catch (error) {
            console.error('Error al obtener datos desde la API:', error);
        }
    }
    const fetchDataFromApi = async () => {
        try {
            // Realiza una solicitud GET a tu API local
            fetch('https://backanalisisdev-production.up.railway.app/list_desembolsos')
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Error al obtener datos de la API');
                    }
                })
                .then(data => {
                    setEmpleados(data.list);
                })
                .catch(error => {
                    console.error('Error al obtener datos:', error);
                });
            // Los datos de respuesta estarán en response.data
            const apiData = response.data;

            // Puedes hacer lo que necesites con los datos, por ejemplo, establecerlos en el estado
            setData(apiData);
        } catch (error) {
            console.error('Error al obtener datos desde la API:', error);
        }
    };

    const optenerRoles = () => {
        fetch('https://backanalisisdev-production.up.railway.app/empleado_list')
            .then((response) => response.json())
            .then((data) => {
                setRoles(data.emple);
            })
            .catch((error) => {
                console.error('Error al obtener los empleados:', error);
                toast.current.show({ severity: 'warn', summary: 'Error', detail: 'Error al obtener los empleados', life: 3000 });
            });
    };
    const onRoleChange = (event) => {
        setSelectedRole(event.value);
    };
    const insertHoras = () => {
        setSubmitted(true);

        let horas = { ...horas_extras };
        horas.empleado_idempleado = selectedRole;

        // Formatea la fecha antes de usarla
        const formattedDate = format(horas.ho_fecha, "yyyy/MM/dd HH:mm:ss");
        horas.ho_fecha = formattedDate
        console.log(horas);
        // Realizar aquí la solicitud de creación a la API utilizando fetch
        fetch('https://backanalisisdev-production.up.railway.app/insertar_horas_extras', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(horas),
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Error al crear el usuario');
                }
            })
            .then(data => {
                // Manejar la respuesta de la API si es necesario
                fetchDataFromApi();
                toast.current.show({ severity: 'success', summary: 'Correcto', detail: 'Horas agregadas correctamente', life: 5000 });
            })
            .catch(error => {
                toast.current.show({ severity: 'war', summary: 'Error', detail: 'Error al insertar las horas', life: 3000 });
            });

        setProductDialog(false);
    };

    const insertHora_Ingreso = () => {
        setSubmitted(true);

        let horas = { ...hora_ingreso };
        horas.empleado_id = selectedRole;

        // Formatea la fecha antes de usarla
        const formattedDate = format(horas.fecha, "yyyy/MM/dd HH:mm:ss");
        const formattedHoraIngreso = format(horas.hora_entrada, "HH:mm:ss");
        const formattedHoraSalida = format(horas.hora_salida, "HH:mm:ss");
        horas.fecha = formattedDate
        horas.hora_entrada = formattedHoraIngreso
        horas.hora_salida = formattedHoraSalida
        // Realizar aquí la solicitud de creación a la API utilizando fetch
        fetch('https://backanalisisdev-production.up.railway.app/insertar_actualizar_ingreso_empleado', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(horas),
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Error al crear el usuario');
                }
            })
            .then(data => {
                // Manejar la respuesta de la API si es necesario
                fetchDataFromApi();
                toast.current.show({ severity: 'success', summary: 'Correcto', detail: 'Hora de Ingreso ó Salída agregada correctamente', life: 5000 });
            })
            .catch(error => {
                toast.current.show({ severity: 'war', summary: 'Error', detail: 'Error al insertar la hora de ingreso ó salída', life: 3000 });
            });

        setInsertarHora_ingresoDialog(false);
    };

    const inserDeudaTienda = () => {
        setSubmitted(true);
        console.log(selectedRole)
        let deuda = { ...tienda_deuda };
        deuda.empleado_idempleado = selectedRole;
        deuda.te_monto_cuotas = tienda_moto_cuotas
        // Realizar aquí la solicitud de creación a la API utilizando fetch
        fetch('https://backanalisisdev-production.up.railway.app/insertar_deuda_tienda', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(deuda),
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Error al realizar la consulta');
                }
            })
            .then(data => {
                // Manejar la respuesta de la API si es necesario
                fetchDataFromApi();
                toast.current.show({ severity: 'success', summary: 'Correcto', detail: 'Deuda en tienda agregada correctamente', life: 5000 });
            })
            .catch(error => {
                toast.current.show({ severity: 'war', summary: 'Error', detail: 'Error al agregar deuda en tienda', life: 3000 });
            });

        setInsertarDeudaTiendaDialog(false);
    };
    const inserPrestamoBanc = () => {
        setSubmitted(true);
        console.log(selectedRole)
        let deuda = { ...prestamo_banc };
        deuda.empleado_idempleado = selectedRole;
        deuda.pr_monto_cuotas = pr_moto_cuotas
        // Realizar aquí la solicitud de creación a la API utilizando fetch
        fetch('https://backanalisisdev-production.up.railway.app/insertar_prestamo_banc', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(deuda),
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Error al realizar la consulta');
                }
            })
            .then(data => {
                // Manejar la respuesta de la API si es necesario
                fetchDataFromApi();
                toast.current.show({ severity: 'success', summary: 'Correcto', detail: 'Prestamo en Banco agregado correctamente', life: 5000 });
            })
            .catch(error => {
                toast.current.show({ severity: 'war', summary: 'Error', detail: 'Error al agregar Prestamo', life: 3000 });
            });

        setInsertprestamoBanc(false);
    };
    const editProduct = (product) => {
        setProduct({ ...product });
        setProductDialog(true);
    }
    const insertarHoras = () => {
        setHoras_extas({ ho_dia_festivo: false, ho_fecha: null, horas_extrascol: 0 })
        setSelectedRole(null)
        setProductDialog(true);
    }
    const insertarHoraIngreso = () => {
        setHora_Ingreso({ fecha: null, hora_entrada: null, hora_salida: null, descripcion: '', empleado_id: null })
        setSelectedRole(null)
        setInsertarHora_ingresoDialog(true);
    }
    const insertarDeudaTienda = () => {
        setTienda_deuda({ te_cuotas: 0, te_monto: 0, te_descripcion: '', te_monto_cuotas: 0 })
        setSelectedRole(null)
        setTienda_moto_cuotas(null)
        setInsertarDeudaTiendaDialog(true);
    }
    const prestamoBanc = () => {
        setPrestamo_banc({ pr_cuotas: 0, pr_monto: 0, pr_descripcion: '', pr_monto_cuotas: 0 })
        setSelectedRole(null)
        setPr_moto_cuotas(null)
        setInsertprestamoBanc(true);
    }

    const deleteProduct = () => {
        let _products = products.filter(val => val.id !== product.id);
        setProducts(_products);
        setDeleteProductDialog(false);
        setProduct(emptyProduct);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'Product Deleted', life: 3000 });
    }


    const optenerListAbono = async () => {
        try {
            // Realiza una solicitud GET a tu API local
            await fetch(`https://backanalisisdev-production.up.railway.app/list_bono`)
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Error al obtener los datos de fam_empleado');
                    }
                })
                .then(data => {
                    console.log(data.bono)
                    // Actualiza el estado con los datos obtenidos
                    setFamEmpleadoDataBono(data.bono);
                })
                .catch(error => {
                    console.error(error);
                    // Maneja el error de la solicitud
                    toast.current.show({ severity: 'warn', summary: 'Error', detail: 'Error al obtener la Lista de Bonos', life: 3000 });
                });
        } catch (error) {
            console.error('Error al obtener datos desde la API:', error);
        }
    };

    useEffect(() => {
        if (famEmpleadoDataBono.length > 0) {
            generarExcel();
        }
    }, [famEmpleadoDataBono]);

    const generarExcel = () => {

        const encabezadoTotal = [[' Bono realizados', '', '', '', '', '', '', '', '', '', '', '', '', '', '']];
        const encabezados = ['Id Bono', 'Bono', 'Fecha', 'Id empleado'];

        const estilo = {
            font: { bold: false },
            alignment: { horizontal: 'center', vertical: 'center' },
            border: {
                top: { style: 'thin', color: { auto: 1 } },
                bottom: { style: 'thin', color: { auto: 1 } },
                left: { style: 'thin', color: { auto: 1 } },
                right: { style: 'thin', color: { auto: 1 } },
            },
        };
        const filas = famEmpleadoDataBono.map(familiar => [
            familiar.idbono,
            familiar.ag_monto,
            format(addDays(new Date(familiar.bn_fecha), 1), 'dd/MM/yy'),
            familiar.empleado_idempleado
        ]);

        const archivoExcel = XLSX.utils.book_new();

        // Resto del código para la hoja de datos del empleado
        const hojaEmpleado = XLSX.utils.aoa_to_sheet([...encabezadoTotal, encabezados, ...filas]);
        hojaEmpleado['!cols'] = [{ width: 20 }, { width: 20 }, { width: 30 }, { width: 30 }];
        hojaEmpleado['!rows'] = filas.map(() => ({ height: 30 }));
        hojaEmpleado['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: encabezados.length - 1 }, style: { font: { size: 26 }, ...estilo } }];

        Object.keys(hojaEmpleado).forEach(key => {
            if (/^[A-Z]+\d+$/.test(key)) {
                hojaEmpleado[key].s = estilo;
            }
        });

        // Agregar la hoja de datos del empleado al libro de trabajo
        XLSX.utils.book_append_sheet(archivoExcel, hojaEmpleado, 'Bono');
        XLSX.writeFile(archivoExcel, 'bonos.xlsx');
    };

    const optenerListAgui = async () => {
        try {
            // Realiza una solicitud GET a tu API local
            await fetch(`https://backanalisisdev-production.up.railway.app/list_aguinaldo`)
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Error al obtener los datos de fam_empleado');
                    }
                })
                .then(data => {
                    console.log(data.agui)
                    // Actualiza el estado con los datos obtenidos
                    setFamEmpleadoDataAgui(data.agui);
                })
                .catch(error => {
                    console.error(error);
                    toast.current.show({ severity: 'warn', summary: 'Error', detail: 'Error al obtener la Lista de Aguinaldos', life: 3000 });
                    // Maneja el error de la solicitud
                });
        } catch (error) {
            console.error('Error al obtener datos desde la API:', error);
        }
    };
    useEffect(() => {
        if (famEmpleadoDataAgui.length > 0) {
            generarExcelAgui();
        }
    }, [famEmpleadoDataAgui]);

    const generarExcelAgui = () => {

        const encabezadoTotal = [[' Aguinaldos realizados', '', '', '', '', '', '', '', '', '', '', '', '', '', '']];
        const encabezados = ['Id Aguinaldo', 'Monto', 'Fecha', 'Id Empleado', 'Nombre empleado'];

        const estilo = {
            font: { bold: false },
            alignment: { horizontal: 'center', vertical: 'center' },
            border: {
                top: { style: 'thin', color: { auto: 1 } },
                bottom: { style: 'thin', color: { auto: 1 } },
                left: { style: 'thin', color: { auto: 1 } },
                right: { style: 'thin', color: { auto: 1 } },
            },
        };
        const filas = famEmpleadoDataAgui.map(familiar => [
            familiar.idaguinaldo,
            familiar.ag_monto,
            format(addDays(new Date(familiar.ag_fecha), 1), 'dd/MM/yy'),
            familiar.empleado_idempleado,
            familiar.bn_nombre
        ]);

        const archivoExcel = XLSX.utils.book_new();

        // Resto del código para la hoja de datos del empleado
        const hojaEmpleado = XLSX.utils.aoa_to_sheet([...encabezadoTotal, encabezados, ...filas]);
        hojaEmpleado['!cols'] = [{ width: 20 }, { width: 20 }, { width: 30 }, { width: 30 }];
        hojaEmpleado['!rows'] = filas.map(() => ({ height: 30 }));
        hojaEmpleado['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: encabezados.length - 1 }, style: { font: { size: 26 }, ...estilo } }];

        Object.keys(hojaEmpleado).forEach(key => {
            if (/^[A-Z]+\d+$/.test(key)) {
                hojaEmpleado[key].s = estilo;
            }
        });

        // Agregar la hoja de datos del empleado al libro de trabajo
        XLSX.utils.book_append_sheet(archivoExcel, hojaEmpleado, 'Aguinaldo');
        XLSX.writeFile(archivoExcel, 'aguinaldo.xlsx');
    };
    const optenerListIggs = async () => {
        await fetch(`https://backanalisisdev-production.up.railway.app/list_iggs`)
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Error al obtener los datos');
                }
            })
            .then(data => {
                console.log(data.iggs)
                // Actualiza el estado con los datos obtenidos

                setFamEmpleadoDataIggs(data.iggs);
            })
            .catch(error => {
                console.error(error);
                toast.current.show({ severity: 'warn', summary: 'Error', detail: 'Error al obtener la Lista de Iggs', life: 3000 });
                // Maneja el error de la solicitud
            });
    };
    useEffect(() => {
        if (famEmpleadoDataIggs.length > 0) {
            generarExcelIggs();
        }
    }, [famEmpleadoDataIggs]);

    const generarExcelIggs = () => {
        const encabezadoTotal = [['Iggs', '', '', '', '', '', '', '', '', '', '', '', '', '', '']];
        const encabezados = ['Id Iggs', 'Cuota Patron', 'Cuota Empleado', 'Id Empleado', 'Fecha'];

        const estilo = {
            font: { bold: false },
            alignment: { horizontal: 'center', vertical: 'center' },
            border: {
                top: { style: 'thin', color: { auto: 1 } },
                bottom: { style: 'thin', color: { auto: 1 } },
                left: { style: 'thin', color: { auto: 1 } },
                right: { style: 'thin', color: { auto: 1 } },
            },
        };
        const filas = famEmpleadoDataIggs.map(familiar => [
            familiar.idigss_cuotas,
            familiar.ig_patron_cuota,
            familiar.ig_empleado_cuota,
            familiar.empleado_idempleado,
            format(addDays(new Date(familiar.ig_fecha), 1), 'dd/MM/yy'),
        ]);

        const archivoExcel = XLSX.utils.book_new();

        // Resto del código para la hoja de datos del empleado
        const hojaEmpleado = XLSX.utils.aoa_to_sheet([...encabezadoTotal, encabezados, ...filas]);
        hojaEmpleado['!cols'] = [{ width: 20 }, { width: 20 }, { width: 30 }, { width: 30 }, { width: 30 }];
        hojaEmpleado['!rows'] = filas.map(() => ({ height: 30 }));
        hojaEmpleado['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: encabezados.length - 1 }, style: { font: { size: 26 }, ...estilo } }];

        Object.keys(hojaEmpleado).forEach(key => {
            if (/^[A-Z]+\d+$/.test(key)) {
                hojaEmpleado[key].s = estilo;
            }
        });

        // Agregar la hoja de datos del empleado al libro de trabajo
        XLSX.utils.book_append_sheet(archivoExcel, hojaEmpleado, 'Iggs');
        XLSX.writeFile(archivoExcel, 'Igss.xlsx');
    };

    const optenerListES = async () => {
        await fetch(`https://backanalisisdev-production.up.railway.app/list_empleado_entra_salida`)
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Error al obtener los datos');
                }
            })
            .then(data => {
                console.log(data.h_e_s)
                // Actualiza el estado con los datos obtenidos

                setFamEmpleadoDataES(data.h_e_s);
                toast.current.show({ severity: 'success', summary: 'Confirmado', detail: 'Reporte de Entradas y Salidas del empleado realizado', life: 5000 });
            })
            .catch(error => {
                console.error(error);
                toast.current.show({ severity: 'warn', summary: 'Error', detail: 'Error al obtener la Lista de Entradas y Salidas', life: 3000 });
                // Maneja el error de la solicitud
            });
    };
    useEffect(() => {
        if (famEmpleadoDataES.length > 0) {
            generarExcelES();
        }
    }, [famEmpleadoDataES]);

    const generarExcelES = () => {
        const encabezadoTotal = [['EMPLEADO HORA ENTRADA Y SALIDA', '', '', '', '', '', '', '', '', '', '', '', '', '', '']];
        const encabezados = ['Id Ingreso', 'Fecha', 'Hora Ingreso', 'Hora Salida','Nombre Completo', 'Dpi', 'Horas Trabajadas', 'Descripción'];

        const estilo = {
            font: { bold: false },
            alignment: { horizontal: 'center', vertical: 'center' },
            border: {
                top: { style: 'thin', color: { auto: 1 } },
                bottom: { style: 'thin', color: { auto: 1 } },
                left: { style: 'thin', color: { auto: 1 } },
                right: { style: 'thin', color: { auto: 1 } },
            },
        };
        const filas = famEmpleadoDataES.map(familiar => [
            familiar.id_ingreso,
            format(addDays(new Date(familiar.fecha), 1), 'dd/MM/yy'),
            familiar.hora_entrada,
            familiar.hora_salida,
            familiar.nombre_completo,
            familiar.em_dpi,
            familiar.horas_trabajadas,
            familiar.descripcion
        ]);

        const archivoExcel = XLSX.utils.book_new();

        // Resto del código para la hoja de datos del empleado
        const hojaEmpleado = XLSX.utils.aoa_to_sheet([...encabezadoTotal, encabezados, ...filas]);
        hojaEmpleado['!cols'] = [{ width: 10 }, { width: 30 }, { width: 30 }, { width: 30 }, { width: 50 },{ width: 40 }, { width: 20 }, { width: 100 }];
        hojaEmpleado['!rows'] = filas.map(() => ({ height: 30 }));
        hojaEmpleado['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: encabezados.length - 1 }, style: { font: { size: 26 }, ...estilo } }];

        Object.keys(hojaEmpleado).forEach(key => {
            if (/^[A-Z]+\d+$/.test(key)) {
                hojaEmpleado[key].s = estilo;
            }
        });

        // Agregar la hoja de datos del empleado al libro de trabajo
        XLSX.utils.book_append_sheet(archivoExcel, hojaEmpleado, 'empleado_entrada_salida');
        XLSX.writeFile(archivoExcel, 'empleado_e_s.xlsx');
    };

    const deleteSelectedProducts = () => {
        let _products = products.filter(val => !selectedProducts.includes(val));
        setProducts(_products);
        setDeleteProductsDialog(false);
        setSelectedProducts(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'Products Deleted', life: 3000 });
    }

    const onCategoryChange = (e) => {
        let _product = { ...horas_extras };
        _product['ho_dia_festivo'] = e.value;
        setHoras_extas(_product);
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _horas = { ...horas_extras };
        _horas[`${name}`] = val;

        setHoras_extas(_horas);
    }
    const onInputChangeDeudaTienda = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _horas = { ...tienda_deuda };
        _horas[`${name}`] = val;

        setTienda_deuda(_horas);
    }

    const onInputNumberChange = (e, name) => {
        const val = e.value || 0;
        let _product = { ...horas_extras };
        _product[`${name}`] = val;

        setHoras_extas(_product);
    }
    const onInputChangeHora_ingreso = (e, name) => {
        const val = e.value || 0;
        let _product = { ...hora_ingreso };
        _product[`${name}`] = val;

        setHora_Ingreso(_product);
    }
    const onInputChangeHora_ingDes = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _horas = { ...hora_ingreso };
        _horas[`${name}`] = val;

        setHora_Ingreso(_horas);
    }
    const onInputNumberDeuda = (e, name) => {
        const val = e.value || 0;
        let _product = { ...tienda_deuda };
        _product[`${name}`] = val;

        setTienda_deuda(_product);
        switch (name) {
            case "te_cuotas":

                setTienda_deuda((prevState) => ({
                    ...prevState, te_cuotas: val
                }))
                setTienda_moto_cuotas(calcularMonto(val, tienda_deuda.te_monto));
                break;
            case "te_monto":
                setTienda_deuda((prevState) => ({
                    ...prevState, te_monto: val
                }))
                setTienda_moto_cuotas(calcularMonto(tienda_deuda.te_cuotas, e.target.value));
                break;
            case "te_descripcion":
                setTienda_deuda((prevState) => ({
                    ...prevState, te_descripcion: val
                }))
                break;
            default:
                break;
        }
    }
    const onInputChangePrestamosBan = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _horas = { ...prestamo_banc };
        _horas[`${name}`] = val;

        setPrestamo_banc(_horas);
    }
    const onInputNumberPrestamo = (e, name) => {
        const val = e.value || 0;
        let _product = { ...prestamo_banc };
        _product[`${name}`] = val;
        console.log(_product)
        setPrestamo_banc(_product);
        switch (name) {
            case "pr_cuotas":

                setPrestamo_banc((prevState) => ({
                    ...prevState, pr_cuotas: val
                }))
                setPr_moto_cuotas(calcularMonto(val, prestamo_banc.pr_monto));
                break;
            case "pr_monto":
                setPrestamo_banc((prevState) => ({
                    ...prevState, pr_monto: val
                }))
                setPr_moto_cuotas(calcularMonto(prestamo_banc.pr_cuotas, val));
                break;
            default:
                break;
        }
    }

    const calcularMonto = (cuota, monto) => {
        return Number((monto / cuota).toFixed(2));
    }

    const items = [
        {
            label: 'INSERTAR HORA INGRESO O SALIDA EMPLEADO',
            icon: 'pi pi-calendar-times',
            command: () => {
                insertarHoraIngreso();
            }
        },
        {
            label: 'INSERTAR HORAS EXTRAS',
            icon: 'pi pi-bolt',
            command: () => {
                insertarHoras();
            }
        },
        {
            label: 'INSERTA CREDITO DE EMPLEADO',
            icon: 'pi pi-dollar',
            command: () => {
                prestamoBanc();
            }
        },
        {
            label: 'INSERTAR DEUDA TIENDA',
            icon: 'pi pi-shopping-bag',
            command: () => {
                insertarDeudaTienda();
            }
        },
    ];
    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>
                <ConfirmDialog visible={visible} onHide={() => setVisible(false)} message="¿Estas seguro de generar el cálculo de quincena?"
                    header="Confirmación" icon="pi pi-exclamation-triangle" accept={accept} reject={reject} />
                <Button onClick={() => setVisible(true)} className="p-button-help mr-2" icon="pi pi-plus" label="NÓMINA QUINCENA" />

                <ConfirmDialog visible={visible2} onHide={() => setVisible2(false)} message="¿Estas segúro de generar el cálculo de Nómina de fin de mes?"
                    header="Confirmación" icon="pi pi-exclamation-triangle" accept={accept2} reject={reject2} />
                <Button onClick={() => setVisible2(true)} className="p-button-info mr-2" icon="pi pi-plus" label="NÓMINA FIN DE MES" />

                <ConfirmDialog visible={visible3} onHide={() => setVisible3(false)} message="¿Estas seguro de generar el cálculo de Bono?"
                    header="Confirmación" icon="pi pi-exclamation-triangle" accept={accept3} reject={reject3} />
                <Button onClick={() => setVisible3(true)} className="p-button-info mr-2" icon="pi pi-plus" label="GENERAR BONO" />

                <ConfirmDialog visible={visible4} onHide={() => setVisible4(false)} message="¿Estas seguro de generar el cálculo de Aguinaldo?"
                    header="Confirmación" icon="pi pi-exclamation-triangle" accept={acceptgenerarAguinaldo} reject={rejectgenerarAguinaldo} />
                <Button onClick={() => setVisible4(true)} className="p-button-help mr-2" icon="pi pi-plus" label="GANERAR AGUINALDO" />

                <ConfirmDialog visible={visible5} onHide={() => setVisible5(false)} message="¿Estas seguro de generar el reporte de iggs?"
                    header="Confirmación" icon="pi pi-exclamation-triangle" accept={accept5} reject={reject5} />
                <Button onClick={() => setVisible5(true)} className="p-button-help mr-2" icon="pi pi-plus" label="REPORTE IGGS" />

                <ConfirmDialog visible={visible6} onHide={() => setVisible6(false)} message="¿Estas seguro de generar el reporte de entradas/salidas del empleado?"
                    header="Confirmación" icon="pi pi-exclamation-triangle" accept={accept6} reject={reject6} />
                <Button onClick={() => setVisible6(true)} className="p-button-warning mr-2" icon="pi pi-plus" label="REPORTE DE EMPLEADO E/S" />

                <Menu model={items} popup ref={menu} id="popup_menu" />
                <Button icon="pi pi-bars" onClick={(event) => menu.current.toggle(event)} aria-controls="popup_menu" aria-haspopup />
            </React.Fragment>
        )
    }


    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-success mr-2" onClick={() => editProduct(rowData)} />
            </React.Fragment>
        );
    }

    const header = (
        <div className="table-header">
            <h5 className="mx-0 my-1">Buscar</h5>
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Buscar..." />
            </span>
        </div>
    );
    const productDialogFooter = (
        <React.Fragment>
            <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Guardar" icon="pi pi-check" className="p-button-text" onClick={insertHoras} />
        </React.Fragment>
    );
    const inserHora_ingresoDialogFooter = (
        <React.Fragment>
            <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={hideDialog4} />
            <Button label="Guardar" icon="pi pi-check" className="p-button-text" onClick={insertHora_Ingreso} />
        </React.Fragment>
    );
    const insertarDeudaTiendaDialogFooter = (
        <React.Fragment>
            <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={hideDialog2} />
            <Button label="Guardar" icon="pi pi-check" className="p-button-text" onClick={inserDeudaTienda} />
        </React.Fragment>
    );
    const insertarPrestamoBancDialogFooter = (
        <React.Fragment>
            <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={hideDialog3} />
            <Button label="Guardar" icon="pi pi-check" className="p-button-text" onClick={inserPrestamoBanc} />
        </React.Fragment>
    );
    const deleteProductDialogFooter = (
        <React.Fragment>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hideDeleteProductDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteProduct} />
        </React.Fragment>
    );
    const deleteProductsDialogFooter = (
        <React.Fragment>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hideDeleteProductsDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteSelectedProducts} />
        </React.Fragment>
    );

    return (
        <div className="datatable-crud-demo">
            <br></br>
            <Toast ref={toast} />

            <div className="card">
                <Toolbar className="mb-4" right={rightToolbarTemplate}></Toolbar>
                <DataTable ref={empleados} value={empleados} selection={selectedProducts} onSelectionChange={(e) => setSelectedProducts(e.value)}
                    dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]}
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} products"
                    globalFilter={globalFilter} header={header} responsiveLayout="scroll">

                    <Column body={actionBodyTemplate} exportable={false} style={{ minWidth: '5rem' }}></Column>
                    <Column field="idpago_nomina" header="Id" sortable style={{ minWidth: '5rem' }}></Column>
                    <Column
                        field="pgn_fecha"
                        header="Fecha Generado"
                        sortable
                        style={{ minWidth: '8rem' }}
                        body={(rowData) => {
                            const formattedDate = new Date(rowData.pgn_fecha).toLocaleDateString();
                            return <span>{formattedDate}</span>;
                        }}
                    />
                    <Column
                        field="pgn_monto"
                        header="Monto"
                        sortable
                        body={(rowData) => (
                            <span style={{ color: 'orange' }}>
                                Q. {parseFloat(rowData.pgn_monto).toLocaleString('es-US', {
                                    minimumFractionDigits: 2,
                                    maximumFractionDigits: 2,
                                })}
                            </span>
                        )}
                    />
                    <Column
                        field="pgn_descuento"
                        header="Descuento"
                        sortable
                        body={(rowData) => (
                            <span style={{ color: 'orange' }}>
                                Q. {parseFloat(rowData.pgn_descuento).toLocaleString('es-US', {
                                    minimumFractionDigits: 2,
                                    maximumFractionDigits: 2,
                                })}
                            </span>
                        )}
                    />
                    <Column field="em_dpi" header="Dpi" sortable style={{ minWidth: '12rem' }}></Column>
                    <Column field="pgn_descripcion" header="Descripción" sortable style={{ minWidth: '20rem' }}></Column>
                </DataTable>
            </div>


            <Dialog visible={insertarDeudaTiendaDialog} style={{ width: '600px' }} header="Ingresar Deuda Tienda" modal className="p-fluid" footer={insertarDeudaTiendaDialogFooter} onHide={hideDialog2}>
                <div className="field">
                    <label htmlFor="name">Escoger un Empleado</label>
                    <Dropdown
                        value={selectedRole}
                        options={roles.map(role => ({
                            ...role,
                            em_nombre_completo: role.em_nombre + ' ' + role.em_pri_apellido + ' ' + role.em_seg_apellido
                        }))
                        }
                        onChange={onRoleChange}
                        optionLabel="em_nombre_completo" // Usar la nueva propiedad em_nombre_completo
                        optionValue="idempleado"
                        filter
                        showClear
                        filterBy="em_nombre_completo"
                        placeholder="Seleccionar Dpi..."
                    />
                </div>

                <div className="formgrid grid">
                    <div className="field col">
                        <label htmlFor="price">Cuotas</label>
                        <InputNumber id="te_cuotas" value={tienda_deuda.te_cuotas} onValueChange={(e) => onInputNumberDeuda(e, 'te_cuotas')} currency="USD" locale="en-US" />
                    </div>
                </div>
                <div className="formgrid grid">
                    <div className="field col">
                        <label htmlFor="price">Monto</label>
                        <InputNumber id="te_monto" value={tienda_deuda.te_monto} onValueChange={(e) => onInputNumberDeuda(e, 'te_monto')} mode="currency" currency="GTQ" locale="en-US" />
                    </div>
                </div>
                <div className="formgrid grid">
                    <div className="field col">
                        <label htmlFor="price">Monto por Cuotas</label>
                        <InputNumber id="horas_extrascol" readOnly value={tienda_moto_cuotas || 0} mode="currency" currency="GTQ" locale="en-US" />
                    </div>
                </div>
                <div className="field grid">
                    <label htmlFor="te_descripcion">Dirección</label>
                    <InputTextarea id="te_descripcion" value={tienda_deuda.te_descripcion} onChange={(e) => onInputChangeDeudaTienda(e, 'te_descripcion')} required rows={1} cols={20} />
                </div>

            </Dialog>

            <Dialog visible={productDialog} style={{ width: '600px' }} header="Ingresar Horas Extras" modal className="p-fluid" footer={productDialogFooter} onHide={hideDialog}>
                <div className="field">
                    <label htmlFor="name">Escoger un Empleado</label>
                    <Dropdown
                        value={selectedRole}
                        options={roles.map(role => ({
                            ...role,
                            em_nombre_completo: role.em_nombre + ' ' + role.em_pri_apellido + ' ' + role.em_seg_apellido
                        }))
                        }
                        onChange={onRoleChange}
                        optionLabel="em_nombre_completo" // Usar la nueva propiedad em_nombre_completo
                        optionValue="idempleado"
                        filter
                        showClear
                        filterBy="em_nombre_completo"
                        placeholder="Seleccionar Dpi..."
                    />
                </div>
                <div className="field col-12 md:col-4">
                    <label htmlFor="description">Fecha y Hora</label>
                    <Calendar id="ho_fecha" dateFormat="dd/mm/yy" value={horas_extras.ho_fecha} onChange={(e) => onInputChange(e, 'ho_fecha')} showIcon showTime showSeconds />
                </div>
                <div className="field">
                    <label className="mb-3">¿Es día festivo?</label>
                    <div className="formgrid grid" style={{ paddingLeft: "20%" }}>
                        <div className="field-radiobutton col-6">
                            <RadioButton
                                inputId="ho_dia_festivo1"
                                name="ho_dia_festivo"
                                value={true}
                                onChange={onCategoryChange}
                                checked={horas_extras.ho_dia_festivo === true}
                            />
                            <label htmlFor="ho_dia_festivo1">Si</label>
                        </div>
                        <div className="field-radiobutton col-6">
                            <RadioButton
                                inputId="ho_dia_festivo2"
                                name="ho_dia_festivo"
                                value={false}
                                onChange={onCategoryChange}
                                checked={horas_extras.ho_dia_festivo === false}
                            />
                            <label htmlFor="ho_dia_festivo2">No</label>
                        </div>
                    </div>
                </div>


                <div className="formgrid grid">
                    <div className="field col">
                        <label htmlFor="price">Horas Trabajadas</label>
                        <InputNumber id="horas_extrascol" value={horas_extras.horas_extrascol} onValueChange={(e) => onInputNumberChange(e, 'horas_extrascol')} currency="USD" locale="en-US" />
                    </div>
                </div>
            </Dialog>

            <Dialog visible={insertarHora_ingresoDialog} style={{ width: '600px' }} header="Ingresar Hora de Ingreso ó Salida del Empleado" modal className="p-fluid" footer={inserHora_ingresoDialogFooter} onHide={hideDialog4}>
                <div className="field">
                    <label htmlFor="name">Escoger un Empleado</label>
                    <Dropdown
                        value={selectedRole}
                        options={roles.map(role => ({
                            ...role,
                            em_nombre_completo: role.em_nombre + ' ' + role.em_pri_apellido + ' ' + role.em_seg_apellido
                        }))
                        }
                        onChange={onRoleChange}
                        optionLabel="em_nombre_completo" // Usar la nueva propiedad em_nombre_completo
                        optionValue="idempleado"
                        filter
                        showClear
                        filterBy="em_nombre_completo"
                        placeholder="Seleccionar Dpi..."
                    />
                </div>
                <div className="field">
                    <label htmlFor="fecha">Fecha de Ingreso</label>
                    <Calendar id="fecha" dateFormat="dd/mm/yy" value={hora_ingreso.fecha} onChange={(e) => onInputChangeHora_ingreso(e, 'fecha')} showIcon />
                </div>
                <div className="formgrid grid">
                    <div className="field col">
                        <label htmlFor="hora_entrada">Hora Ingreso</label>
                        <Calendar id="hora_entrada" value={hora_ingreso.hora_entrada} onChange={(e) => onInputChangeHora_ingreso(e, 'hora_entrada')} timeOnly readOnlyInput/>
                    </div>
                </div>
                <div className="formgrid grid">
                    <div className="field col">
                        <label htmlFor="hora_entrada">Hora Salida</label>
                        <Calendar id="hora_salida" value={hora_ingreso.hora_salida} onChange={(e) => onInputChangeHora_ingreso(e, 'hora_salida')} timeOnly readOnlyInput/>
                    </div>
                </div>
                <div className="field">
                    <label htmlFor="te_descripcion">Descripción  - Aquí se puede agregar cualquier dato necesario -</label>
                    <InputTextarea id="descripcion" value={hora_ingreso.descripcion} onChange={(e) => onInputChangeHora_ingDes(e, 'descripcion')} required rows={1} cols={20} />
                </div>
            </Dialog>

            <Dialog visible={insertprestamoBanc} style={{ width: '600px' }} header="Ingresar Credito Empleado" modal className="p-fluid" footer={insertarPrestamoBancDialogFooter} onHide={hideDialog3}>
                <div className="field">
                    <label htmlFor="name">Escoger un Empleado</label>
                    <Dropdown
                        value={selectedRole}
                        options={roles.map(role => ({
                            ...role,
                            em_nombre_completo: role.em_nombre + ' ' + role.em_pri_apellido + ' ' + role.em_seg_apellido
                        }))
                        }
                        onChange={onRoleChange}
                        optionLabel="em_nombre_completo" // Usar la nueva propiedad em_nombre_completo
                        optionValue="idempleado"
                        filter
                        showClear
                        filterBy="em_nombre_completo"
                        placeholder="Seleccionar Dpi..."
                    />
                </div>
                <div className="formgrid grid">
                    <div className="field col">
                        <label htmlFor="price">Cuotas</label>
                        <InputNumber id="pr_cuotas" value={prestamo_banc.pr_cuotas} onValueChange={(e) => onInputNumberPrestamo(e, 'pr_cuotas')} currency="USD" locale="en-US" />
                    </div>
                </div>
                <div className="formgrid grid">
                    <div className="field col">
                        <label htmlFor="price">Monto</label>
                        <InputNumber id="pr_monto" value={prestamo_banc.pr_monto} onValueChange={(e) => onInputNumberPrestamo(e, 'pr_monto')} mode="currency" currency="GTQ" locale="en-US" />
                    </div>
                </div>
                <div className="formgrid grid">
                    <div className="field col">
                        <label htmlFor="pr_moto_cuotas">Monto por Cuotas</label>
                        <InputNumber id="pr_moto_cuotas" readOnly value={pr_moto_cuotas || 0} mode="currency" currency="GTQ" locale="en-US" />
                    </div>
                </div>
                <div className="field grid">
                    <label htmlFor="te_descripcion">Descripción</label>
                    <InputTextarea id="pr_descripcion" value={prestamo_banc.pr_descripcion} onChange={(e) => onInputChangePrestamosBan(e, 'pr_descripcion')} required rows={1} cols={20} />
                </div>
            </Dialog>

            <Dialog visible={deleteProductDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteProductDialogFooter} onHide={hideDeleteProductDialog}>
                <div className="confirmation-content">
                    <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                    {product && <span>Are you sure you want to delete <b>{product.name}</b>?</span>}
                </div>
            </Dialog>

            <Dialog visible={deleteProductsDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteProductsDialogFooter} onHide={hideDeleteProductsDialog}>
                <div className="confirmation-content">
                    <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                    {product && <span>Are you sure you want to delete the selected products?</span>}
                </div>
            </Dialog>
        </div>
    );
}
