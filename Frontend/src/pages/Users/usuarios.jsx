
import React, { useState, useEffect, useRef } from 'react';
import { classNames } from 'primereact/utils';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { ProductService } from '../../components/service/ProductService';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Rating } from 'primereact/rating';
import { Toolbar } from 'primereact/toolbar';
import { InputTextarea } from 'primereact/inputtextarea';
import { RadioButton } from 'primereact/radiobutton';
import { InputNumber } from 'primereact/inputnumber';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Password } from 'primereact/password';
import axios from 'axios'; // Si estás utilizando axios

export default function Users() {

    let emptyProduct = {
        id: null,
        name: '',
        image: null,
        description: '',
        category: null,
        price: 0,
        quantity: 0,
        rating: 0,
        inventoryStatus: 'INSTOCK'
    };
    let emtyUsers = {
        id: null,
        Nombre: '',
        Correo: '',
        Password: '',
        role_id: null
    };

    const [products, setProducts] = useState(null);
    const [users, setUsers] = useState(null);
    const [user, setUser] = useState(emtyUsers);
    const [productDialog, setProductDialog] = useState(false);
    const [deleteUserDialog, setDeleteUserDialog] = useState(false);
    const [deleteProductsDialog, setDeleteProductsDialog] = useState(false);
    const [product, setProduct] = useState(emptyProduct);
    const [selectedProducts, setSelectedProducts] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const [roles, setRoles] = useState([]);
    const [selectedRole, setSelectedRole] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);
    const [loading, setLoading] = useState(false);
    const productService = new ProductService();

    useEffect(() => {
        productService.getProducts().then(data => setProducts(data));
        fetchDataFromApi();
        optenerRoles();
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const openNew = () => {
        setUser(emtyUsers)
        setSubmitted(false);
        setProductDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setProductDialog(false);
    }

    const hideDeleteProductDialog = () => {
        setDeleteUserDialog(false);
    }
    const onCityChange = (e) => {
        setSelectedCity1(e.value);
    }

    const hideDeleteProductsDialog = () => {
        setDeleteProductsDialog(false);
    }
    const fetchDataFromApi = () => {
      setLoading(true);
        try {
            fetch('https://backanalisisdev-production.up.railway.app/users')
                .then(response => {
                    if (response.ok) {
                        return response.json();
                        setLoading(false);
                    } else {
                        throw new Error('Error al obtener datos de la API');
                    }
                })
                .then(data => {
                    console.log(data.users);
                    setLoading(false);
                    setUsers(data.users);
                })
                .catch(error => {
                    console.error('Error al obtener datos:', error);
                });
        } catch (error) {
            console.error('Error al obtener datos desde la API:', error);
        }
    };
    const onRoleChange = (event) => {
      setSelectedRole(event.value);
    };

    const optenerRoles = () => {
      fetch('https://backanalisisdev-production.up.railway.app/roles')
        .then((response) => response.json())
        .then((data) => {
          setRoles(data.roles);
        })
        .catch((error) => {
          console.error('Error al obtener roles:', error);
        });
    };  
    const saveProduct = () => {
        setSubmitted(true);
        if (user.Nombre.trim() || user.Correo.trim() || user.Password.trim()) {
          console.log(selectedRole)
          let _users = [...users]; 
          let _user = {...user};    
          _user.role_id = selectedRole; 
          if (user.id) {
            // Si `users.id` existe, entonces estamos actualizando un usuario existente
            const index = findIndexById(user.id);
            _users[index] = _user;
            console.log(_user)
            // Realizar aquí la solicitud de actualización a la API utilizando fetch
            fetch(`https://backendanalisis2-production.up.railway.app/update_users/${_user.id}`, {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(_user),
            })
              .then(response => {
                if (response.ok) {
                  fetchDataFromApi();
                  return response.json();
                } else {
                  throw new Error('Error al actualizar el usuario');
                }
              })
              .then(data => {
                fetchDataFromApi();
              })
              .catch(error => {
                console.error('Error al actualizar el usuario:', error);
              });
            toast.current.show({ severity: 'success', summary: 'Exitoso', detail: 'Usuario Actualizado', life: 3000 });
          } else {
            // Si `users.id` no existe, estamos creando un nuevo usuario
            _user.id = createId();
            _users.push(_user);
            console.log(_user)
            // Realizar aquí la solicitud de creación a la API utilizando fetch
            fetch('https://backanalisisdev-production.up.railway.app/create_users', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(_user),
            })
              .then(response => {
                if (response.ok) {
                  return response.json();
                } else {
                  throw new Error('Error al crear el usuario');
                }
              })
              .then(data => {
                // Manejar la respuesta de la API si es necesario
                fetchDataFromApi();
              })
              .catch(error => {
                console.error('Error al crear el usuario:', error);
              });
            toast.current.show({ severity: 'success', summary: 'Exitoso', detail: 'Usuario Creado', life: 3000 });
          }
          setProductDialog(false);
        }
      };
      

      const editProduct =  (userpara) => {
        console.log(userpara);
         setUser({
          id: userpara.id,
          Nombre: userpara.nombre_usuario,
          Correo: userpara.email,
          role_id:  userpara.role_id
        });
        setSelectedRole(userpara.role_id)
        setProductDialog(true);
      }
      
    const confirmDeleteUser =  (userToDelete) => {
        console.log(userToDelete)
        setUser({
            id: userToDelete.id,
            Nombre:userToDelete.nombre_usuario,
            Correo: userToDelete.email
        })
        /* setUser(userToDelete); */
        setDeleteUserDialog(true);
      }
    
      const deleteUser = () => {
        // Realiza una solicitud al servidor para eliminar el usuario
        fetch(`https://backanalisisdev-production.up.railway.app/delete_users/${user.id}`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => {
            if (response.ok) {
              // Elimina el usuario de la lista en el cliente
              const updatedUsers = users.filter((u) => u.id !== user.id);
              setUsers(updatedUsers);
              setDeleteUserDialog(false);
              setUser(emtyUsers);
              toast.current.show({ severity: 'success', summary: 'Successful', detail: 'User Deleted', life: 3000 });
            } else {
              // Maneja errores si es necesario
              console.error('Error al eliminar el usuario:', response.status);
            }
          })
          .catch((error) => {
            console.error('Error al eliminar el usuario:', error);
          });
      }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < users.length; i++) {
            if (users[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const cities = [
        { name: 'New York', code: 'NY' },
        { name: 'Rome', code: 'RM' },
        { name: 'London', code: 'LDN' },
        { name: 'Istanbul', code: 'IST' },
        { name: 'Paris', code: 'PRS' }
    ];

    const deleteSelectedProducts = () => {
        let _products = products.filter(val => !selectedProducts.includes(val));
        setProducts(_products);
        setDeleteProductsDialog(false);
        setSelectedProducts(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'Products Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _user = { ...user };
        _user[`${name}`] = val;

        setUser(_user);
    }


    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                </React.Fragment>
        )
    }


    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-success mr-2" onClick={() => editProduct(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-warning" onClick={() => confirmDeleteUser(rowData)} />
            </React.Fragment>
        );
    }

    const header = (
        <div className="table-header">
            <h5 className="mx-0 my-1">Usuarios</h5>
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );
    const productDialogFooter = (
        <React.Fragment>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveProduct} />
        </React.Fragment>
    );
    const deleteProductDialogFooter = (
        <React.Fragment>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hideDeleteProductDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteUser} />
        </React.Fragment>
    );
    const deleteProductsDialogFooter = (
        <React.Fragment>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hideDeleteProductsDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteSelectedProducts} />
        </React.Fragment>
    );

    return (
        <div className="datatable-crud-demo">
            <br></br>
            <Toast ref={toast} />

            <div className="card">
                <Toolbar className="mb-4" left={leftToolbarTemplate} ></Toolbar>

                <DataTable ref={dt} value={users} selection={selectedProducts} onSelectionChange={(e) => setSelectedProducts(e.value)}
                    dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} loading={loading}
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} products"
                    globalFilter={globalFilter} header={header} responsiveLayout="scroll">
                    <Column field="id" header="Id" sortable style={{ minWidth: '2rem' }}></Column>
                    <Column field="nombre_usuario" header="Nombre" sortable style={{ minWidth: '20rem' }}></Column>
                    <Column field="email" header="Correo"  sortable style={{ minWidth: '12rem' }}></Column>
                    <Column field="nombre_rol" header="Rol"  sortable style={{ minWidth: '8rem' }}></Column>
                    <Column body={actionBodyTemplate} exportable={false} style={{ minWidth: '8rem' }}></Column>
                </DataTable>
            </div>

            <Dialog visible={productDialog} style={{ width: '450px' }} header="Datos del Usuario" modal className="p-fluid" footer={productDialogFooter} onHide={hideDialog}>
                <div className="field">
                    <label htmlFor="nombre">Nombre</label>
                    <InputText id="Nombre" value={user.Nombre} onChange={(e) => onInputChange(e, 'Nombre')} required autoFocus className={classNames({ 'p-invalid': submitted && !user.Nombre })} />
                    {submitted && !user.Nombre && <small className="p-error">Nombre es requerido.</small>}
                </div>
                <div className="field">
                    <label htmlFor="name">Correo</label>
                    <InputText id="Correo" value={user.Correo} onChange={(e) => onInputChange(e, 'Correo')} required autoFocus className={classNames({ 'p-invalid': submitted && !user.Correo })} />
                    {submitted && !user.Correo && <small className="p-error">Correo es requerido.</small>}
                </div>
                <div className="field">
                    <label htmlFor="name">Contraseña</label>
                    <Password  id="Password" value={user.Password} onChange={(e) => onInputChange(e, 'Password')} toggleMask className={classNames({ 'p-invalid': submitted && !user.Password })}/>
                    {submitted && !user.Password && <small className="p-error">Contraseña es requerido.</small>}
                </div>
                <div className="field">
                <label htmlFor="name">Rol</label>
                  <Dropdown
                    value={selectedRole}
                    options={roles}
                    onChange={onRoleChange}
                    optionLabel="nombre_rol"
                    optionValue="id"
                    placeholder="Selecciona un rol"
                  />
                </div>
            </Dialog>

            <Dialog visible={deleteUserDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteProductDialogFooter} onHide={hideDeleteProductDialog}>
                <div className="confirmation-content">
                    <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                    {user && <span>Estas seguro de eliminar al Usurio <b>{user.Nombre}</b>?</span>}
                </div>
            </Dialog>

            <Dialog visible={deleteProductsDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteProductsDialogFooter} onHide={hideDeleteProductsDialog}>
                <div className="confirmation-content">
                    <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                    {product && <span>Are you sure you want to delete the selected products?</span>}
                </div>
            </Dialog>
        </div>
    );
}
